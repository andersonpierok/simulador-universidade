class CreateTablePessoas < ActiveRecord::Migration
  def change
	create_table :pessoas do |t|
	  t.boolean  :professor
	  t.integer  :matricula
	  t.string   :cpf
	  t.string   :nome
	  t.integer  :carga_horaria

	  t.timestamps
	end
  end
end
