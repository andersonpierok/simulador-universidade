class CreateTableEndereco < ActiveRecord::Migration
	def change
		create_table  :enderecos do |t|
			t.integer :pessoa_id
			t.string  :rua
			t.integer :numero
			t.string  :complemento
			t.string  :cidade
			t.string  :uf
			t.string  :cep
			
			t.timestamps
		end
		add_foreign_key(:enderecos, :pessoas, column: :pessoa_id)
	end
end
