class CreateTableContato < ActiveRecord::Migration
	def change
		create_table  :contatos do |t|
			t.integer :pessoa_id
			t.string  :telefone
			t.string  :email
			
			t.timestamps
		end
		add_foreign_key(:contatos, :pessoas, column: :pessoa_id)
	end
end
