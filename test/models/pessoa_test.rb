# encoding: UTF-8

require 'test_helper'

describe Pessoa do
	subject    { FactoryGirl.build(:pessoa) }
	let(:mock) { MiniTest::Mock.new }
	
	context "Regras do modelo" do
		it { must allow_mass_assignment_of(:professor)           }
		it { must allow_mass_assignment_of(:matricula)           }
		it { must allow_mass_assignment_of(:cpf)                 }
		it { must allow_mass_assignment_of(:nome)                }
		it { must allow_mass_assignment_of(:carga_horaria)       }

		it { have_many(:contatos).class_name('Contato').with_foreign_key(:pessoa_id) }
		it { have_many(:enderecos).class_name('Endereco').with_foreign_key(:pessoa_id) }
	end
	
end