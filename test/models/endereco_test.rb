# encoding: UTF-8

require 'test_helper'

describe Endereco do
	subject    { FactoryGirl.build(:endereco) }
	let(:mock) { MiniTest::Mock.new }
	
	context "Regras do modelo" do
		it { must allow_mass_assignment_of(:pessoa_id)   }
		it { must allow_mass_assignment_of(:pessoa)      }
		it { must allow_mass_assignment_of(:rua)         }
		it { must allow_mass_assignment_of(:numero)      }
		it { must allow_mass_assignment_of(:complemento) }
		it { must allow_mass_assignment_of(:cidade)      }
		it { must allow_mass_assignment_of(:uf)          }
		it { must allow_mass_assignment_of(:cep)         }

		it { must belong_to(:pessoa).class_name('Pessoa').with_foreign_key(:pessoa_id) }
	end
	
end