# encoding: UTF-8

require 'test_helper'

describe Contato do
	subject    { FactoryGirl.build(:contato) }
	let(:mock) { MiniTest::Mock.new }
	
	context "Regras do modelo" do
		it { must allow_mass_assignment_of(:pessoa_id) }
		it { must allow_mass_assignment_of(:pessoa)    }
		it { must allow_mass_assignment_of(:telefone)  }
		it { must allow_mass_assignment_of(:email)     }

		it { must belong_to(:pessoa).class_name('Pessoa').with_foreign_key(:pessoa_id) }
	end
	
end