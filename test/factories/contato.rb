# encoding: UTF-8

FactoryGirl.define do
	factory :contato do
		pessoa { FactoryGirl.build(:pessoa) }
		telefone '49-3333.3333'
		email 'jose@da_silva.com.br'
	end
end