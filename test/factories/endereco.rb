# encoding: UTF-8

FactoryGirl.define do
	factory :endereco do
		pessoa { FactoryGirl.build(:pessoa) }
		rua "Principal"
		numero 123
		complemento "E"
		cidade "Chapecó"
		uf "SC"
		cep "89.800-000"
	end
end