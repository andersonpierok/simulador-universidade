# encoding: UTF-8

FactoryGirl.define do
	factory :pessoa do
		professor false
		matricula 333
		cpf '000.000.000-55'
		nome 'José da Silva'
		carga_horaria nil
	end
end