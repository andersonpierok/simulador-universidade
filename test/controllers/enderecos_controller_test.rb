# encoding: UTF-8

require "test_helper"

describe EnderecosController do
	let(:mock) { MiniTest::Mock.new }
	subject { @controller }
	let(:endereco) {FactoryGirl.build(:endereco)}
	
	context "Ações controller" do
		context "Ação Index" do
			it "No index deve carregar todos os enderecos cadastrados" do
				mock.expect(:carrega_enderecos, true, [Endereco.all])
				get(:index)
				must_respond_with(:success)
				assert mock.carrega_enderecos(assigns(:enderecos))
			end

			it "Deve setar o menu ativo como Enderecos" do
				get(:index)
				assigns(:menu_ativo).must_equal "Enderecos"
			end
		end

		context "Ação Show" do
			it "No show deve carregar o endereco" do
				Endereco.stubs(:find_by_id).with("123").returns(endereco)
				endereco.id = "123"
				get(:show, id: "123")
				must_respond_with(:success)
				assigns(:endereco).must_equal endereco
			end
		end

		context "Ação New" do
			it "No new deve instanciar um novo endereco" do
				get(:new)
				must_respond_with(:success)
				assigns(:endereco).new_record?.must_equal true
			end

			it "Deve carregar as pessoas" do
				mock.expect(:carrega_pessoas, true, [Pessoa.all])
				get(:new)
				assert mock.carrega_pessoas(assigns(:pessoas))
			end
		end

		context "Ação Edit" do
			it "No edit deve buscar o endereco" do
				Endereco.stubs(:find_by_id).with("123").returns(endereco)
				endereco.id = "123"
				get(:edit, id: "123")
				must_respond_with(:success)
				assigns(:endereco).must_equal endereco
			end

			it "Deve carregar as pessoas" do
				Endereco.stubs(:find_by_id).with("123").returns(endereco)
				endereco.id = "123"
				mock.expect(:carrega_pessoas, true, [Pessoa.all])
				get(:edit, id: "123")
				assert mock.carrega_pessoas(assigns(:pessoas))
			end
		end

		context "Ação Create" do
			it "No create deve instanciar um novo endereco com os parametros passados" do
				endereco.stubs(:save).returns(true)
				Endereco.expects(:new).with('rua' => "3333333").returns(endereco)
				post(:create, endereco: { rua: "3333333" })
				must_redirect_to(endereco)
			end

			it "Quando não salva" do
				endereco.stubs(:save).returns(false)
				Endereco.expects(:new).with('rua' => "3333333").returns(endereco)
				post(:create, endereco: { rua: "3333333" })
				must_render_template('new')
			end
		end

		context "Ação Update" do
			it "No update deve atualizar um endereco com os parametros passados" do
				Endereco.stubs(:find_by_id).with("123").returns(endereco)
				endereco.id = "123"
				endereco.expects(:update_attributes).with('rua' => "3333333" ).returns(true)
				post(:update, id: 123, endereco: { rua: "3333333"})
				must_redirect_to(endereco)
			end

			it "Quando não salva" do
				Endereco.stubs(:find_by_id).with("123").returns(endereco)
				endereco.id = "123"
				endereco.expects(:update_attributes).with('rua' => "3333333" ).returns(false)
				post(:update, id: 123, endereco: { rua: "3333333"})
				must_render_template('edit')
			end
		end	

		context "Ação destroy" do
			it "Excluindo" do
				Endereco.stubs(:find_by_id).with("123").returns(endereco)
				endereco.id = "123"
				delete(:destroy, id: 123)
				must_redirect_to(enderecos_path)
			end
		end
	end
end