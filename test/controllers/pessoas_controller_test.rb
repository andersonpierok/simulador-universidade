# encoding: UTF-8

require "test_helper"

describe PessoasController do
	let(:mock) { MiniTest::Mock.new }
	subject { @controller }
	let(:pessoa) {FactoryGirl.build(:pessoa)}
	
	context "Ações controller" do
		context "Ação Index" do
			it "No index deve carregar todas as pessoas cadastradas" do
				mock.expect(:carrega_pessoas, true, [Pessoa.all])
				get(:index)
				must_respond_with(:success)
				assert mock.carrega_pessoas(assigns(:pessoas))
			end

			it "Deve setar o menu ativo como Pessoas" do
				get(:index)
				assigns(:menu_ativo).must_equal "Pessoas"
			end
		end

		context "Ação Show" do
			it "No show deve carregar a pessoa" do
				Pessoa.stubs(:find_by_id).with("123").returns(pessoa)
				pessoa.id = "123"
				get(:show, id: "123")
				must_respond_with(:success)
				assigns(:pessoa).must_equal pessoa
			end
		end

		context "Ação New" do
			it "No new deve instanciar uma nova pessoa" do
				get(:new)
				must_respond_with(:success)
				assigns(:pessoa).new_record?.must_equal true
			end
		end

		context "Ação Edit" do
			it "No edit deve buscar a pessoa" do
				Pessoa.stubs(:find_by_id).with("123").returns(pessoa)
				pessoa.id = "123"
				get(:edit, id: "123")
				must_respond_with(:success)
				assigns(:pessoa).must_equal pessoa
			end
		end

		context "Ação Create" do
			it "No create deve instanciar uma nova pessoa com os parametros passados" do
				pessoa.stubs(:save).returns(true)
				Pessoa.expects(:new).with('nome' => "Anderson", 'professor' => false, 'matricula' => "123456", 'cpf' => "000.000.000-00", 'carga_horaria' => "0" ).returns(pessoa)
				post(:create, pessoa: { nome: "Anderson", professor: false, matricula: 123456, cpf: "000.000.000-00", carga_horaria: 0 })
				must_redirect_to(pessoa)
			end

			it "Quando não salva" do
				pessoa.stubs(:save).returns(false)
				Pessoa.expects(:new).with('nome' => "Anderson", 'professor' => false, 'matricula' => "123456", 'cpf' => "000.000.000-00", 'carga_horaria' => "0" ).returns(pessoa)
				post(:create, pessoa: { nome: "Anderson", professor: false, matricula: 123456, cpf: "000.000.000-00", carga_horaria: 0 })
				must_render_template('new')
			end
		end

		context "Ação Update" do
			it "No update deve atualizar uma nova pessoa com os parametros passados" do
				Pessoa.stubs(:find_by_id).with("123").returns(pessoa)
				pessoa.id = "123"
				pessoa.expects(:update_attributes).with('nome' => "Anderson", 'professor' => false, 'matricula' => "123456", 'cpf' => "000.000.000-00", 'carga_horaria' => "0" ).returns(true)
				post(:update, id: 123, pessoa: { nome: "Anderson", professor: false, matricula: 123456, cpf: "000.000.000-00", carga_horaria: 0 })
				must_redirect_to(pessoa)
			end

			it "Quando não salva" do
				Pessoa.stubs(:find_by_id).with("123").returns(pessoa)
				pessoa.id = "123"
				pessoa.expects(:update_attributes).with('nome' => "Anderson", 'professor' => false, 'matricula' => "123456", 'cpf' => "000.000.000-00", 'carga_horaria' => "0" ).returns(false)
				post(:update, id: 123, pessoa: { nome: "Anderson", professor: false, matricula: 123456, cpf: "000.000.000-00", carga_horaria: 0 })
				must_render_template('edit')
			end
		end	

		context "Ação destroy" do
			it "Excluindo" do
				Pessoa.stubs(:find_by_id).with("123").returns(pessoa)
				pessoa.id = "123"
				delete(:destroy, id: 123)
				must_redirect_to(pessoas_path)
			end
		end
	end
end