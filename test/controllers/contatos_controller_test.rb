# encoding: UTF-8

require "test_helper"

describe ContatosController do
	let(:mock) { MiniTest::Mock.new }
	subject { @controller }
	let(:contato) {FactoryGirl.build(:contato)}
	
	context "Ações controller" do
		context "Ação Index" do
			it "No index deve carregar todos os contatos cadastrados" do
				mock.expect(:carrega_contatos, true, [Contato.all])
				get(:index)
				must_respond_with(:success)
				assert mock.carrega_contatos(assigns(:contatos))
			end

			it "Deve setar o menu ativo como Contatos" do
				get(:index)
				assigns(:menu_ativo).must_equal "Contatos"
			end
		end

		context "Ação Show" do
			it "No show deve carregar o contato" do
				Contato.stubs(:find_by_id).with("123").returns(contato)
				contato.id = "123"
				get(:show, id: "123")
				must_respond_with(:success)
				assigns(:contato).must_equal contato
			end
		end

		context "Ação New" do
			it "No new deve instanciar um novo contato" do
				get(:new)
				must_respond_with(:success)
				assigns(:contato).new_record?.must_equal true
			end

			it "Deve carregar as pessoas" do
				mock.expect(:carrega_pessoas, true, [Pessoa.all])
				get(:new)
				assert mock.carrega_pessoas(assigns(:pessoas))
			end
		end

		context "Ação Edit" do
			it "No edit deve buscar o contato" do
				Contato.stubs(:find_by_id).with("123").returns(contato)
				contato.id = "123"
				get(:edit, id: "123")
				must_respond_with(:success)
				assigns(:contato).must_equal contato
			end

			it "Deve carregar as pessoas" do
				Contato.stubs(:find_by_id).with("123").returns(contato)
				contato.id = "123"
				mock.expect(:carrega_pessoas, true, [Pessoa.all])
				get(:edit, id: "123")
				assert mock.carrega_pessoas(assigns(:pessoas))
			end
		end

		context "Ação Create" do
			it "No create deve instanciar um novo contato com os parametros passados" do
				contato.stubs(:save).returns(true)
				Contato.expects(:new).with('telefone' => "3333333").returns(contato)
				post(:create, contato: { telefone: "3333333" })
				must_redirect_to(contato)
			end

			it "Quando não salva" do
				contato.stubs(:save).returns(false)
				Contato.expects(:new).with('telefone' => "3333333").returns(contato)
				post(:create, contato: { telefone: "3333333" })
				must_render_template('new')
			end
		end

		context "Ação Update" do
			it "No update deve atualizar uma nova pessoa com os parametros passados" do
				Contato.stubs(:find_by_id).with("123").returns(contato)
				contato.id = "123"
				contato.expects(:update_attributes).with('telefone' => "3333333" ).returns(true)
				post(:update, id: 123, contato: { telefone: "3333333"})
				must_redirect_to(contato)
			end

			it "Quando não salva" do
				Contato.stubs(:find_by_id).with("123").returns(contato)
				contato.id = "123"
				contato.expects(:update_attributes).with('telefone' => "3333333" ).returns(false)
				post(:update, id: 123, contato: { telefone: "3333333"})
				must_render_template('edit')
			end
		end	

		context "Ação destroy" do
			it "Excluindo" do
				Contato.stubs(:find_by_id).with("123").returns(contato)
				contato.id = "123"
				delete(:destroy, id: 123)
				must_redirect_to(contatos_path)
			end
		end
	end
end