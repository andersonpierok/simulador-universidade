class ContatosController < ApplicationController

	before_filter :menu_ativo

	def index
		@contatos = Contato.all
	end

	def show
		@contato = Contato.find_by_id(params[:id])
	end

	def new
		@contato = Contato.new
		@pessoas = Pessoa.all
	end

	def edit
		@contato = Contato.find_by_id(params[:id])
		@pessoas = Pessoa.all
	end

	def create
		@contato = Contato.new(params[:contato])

		if @contato.save
			redirect_to @contato
		else
			render action: "new"
		end
	end

	def update
		@contato = Contato.find_by_id(params[:id])

		if @contato.update_attributes(params[:contato])
			redirect_to @contato
		else
			render action: "edit"
		end
	end

	def destroy
		@contato = Contato.find_by_id(params[:id])
		@contato.destroy

		redirect_to contatos_url 
	end

	private

	def menu_ativo
		@menu_ativo = "Contatos"
	end
end