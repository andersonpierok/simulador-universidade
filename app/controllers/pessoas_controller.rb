class PessoasController < ApplicationController

	before_filter :menu_ativo

	def index
		@pessoas = Pessoa.all
	end

	def show
		@pessoa = Pessoa.find_by_id(params[:id])
	end

	def new
		@pessoa = Pessoa.new
	end

	def edit
		@pessoa = Pessoa.find_by_id(params[:id])
	end

	def create
		@pessoa = Pessoa.new(params[:pessoa])

		if @pessoa.save
			redirect_to @pessoa
		else
			render action: "new"
		end
	end

	def update
		@pessoa = Pessoa.find_by_id(params[:id])

		if @pessoa.update_attributes(params[:pessoa])
			redirect_to @pessoa
		else
			render action: "edit"
		end
	end

	def destroy
		@pessoa = Pessoa.find_by_id(params[:id])
		@pessoa.destroy

		redirect_to pessoas_url 
	end

	private

	def menu_ativo
		@menu_ativo = "Pessoas"
	end
end