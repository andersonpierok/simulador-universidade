class EnderecosController < ApplicationController

	before_filter :menu_ativo

	def index
		@enderecos = Endereco.all
	end

	def show
		@endereco = Endereco.find_by_id(params[:id])
	end

	def new
		@endereco = Endereco.new
		@pessoas = Pessoa.all
	end

	def edit
		@endereco = Endereco.find_by_id(params[:id])
		@pessoas = Pessoa.all
	end

	def create
		@endereco = Endereco.new(params[:endereco])

		if @endereco.save
			redirect_to @endereco
		else
			render action: "new"
		end
	end

	def update
		@endereco = Endereco.find_by_id(params[:id])

		if @endereco.update_attributes(params[:endereco])
			redirect_to @endereco
		else
			render action: "edit"
		end
	end

	def destroy
		@endereco = Endereco.find_by_id(params[:id])
		@endereco.destroy

		redirect_to enderecos_url 
	end

	private

	def menu_ativo
		@menu_ativo = "Enderecos"
	end
end