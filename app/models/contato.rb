# encoding: UTF-8
class Contato < ActiveRecord::Base
	self.table_name = "contatos"

	attr_accessible :pessoa_id, :pessoa, :telefone, :email

	belongs_to :pessoa, class_name: "Pessoa", foreign_key: :pessoa_id

end