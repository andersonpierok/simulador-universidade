# encoding: UTF-8
class Pessoa < ActiveRecord::Base
	self.table_name = "pessoas"

	attr_accessible :professor, :matricula, :cpf, :nome, :carga_horaria

	has_many :contatos, class_name: "Contato", foreign_key: :pessoa_id
	has_many :enderecos, class_name: "Endereco", foreign_key: :pessoa_id

end