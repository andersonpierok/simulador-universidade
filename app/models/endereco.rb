# encoding: UTF-8
class Endereco < ActiveRecord::Base
	self.table_name = "enderecos"

	attr_accessible :pessoa_id, :pessoa, :rua, :numero, :complemento, :cidade, :uf, :cep

	belongs_to :pessoa, class_name: "Pessoa", foreign_key: :pessoa_id

end