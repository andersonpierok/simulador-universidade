require "rake/testtask"
require 'rubygems'
namespace :test do
	Rake::TestTask.new(:models) do |t|    
		t.libs << "test"
		t.pattern = 'test/models/**/*_test.rb'
		t.verbose = true    
	end

	Rake::TestTask.new(:controllers) do |t|    
		t.libs << "test"
		t.pattern = 'test/controllers/**/*_test.rb'
		t.verbose = true    
	end
	
	Rake::TestTask.new(:all) do |t|
		t.libs << "test"
		t.test_files = FileList[
			'test/controllers/**/*_test.rb',
			'test/models/**/*_test.rb']
		t.verbose = true
	end
end

class Rake::Task
	def overwrite(&block)
		@actions.clear
		enhance(&block)
	end
end

Rake::Task["test:run"].overwrite do
	errors = %w(test:models test:controllers test:units test:functionals test:integration).collect do |task|
		begin
			Rake::Task[task].invoke
			nil
		rescue => e
			{ :task => task, :exception => e }
		end
	end.compact

	if errors.any?
		puts errors.map { |e| "Errors na tarefa de minitests #{e[:task]}! #{e[:exception].inspect}" }.join("\n")
		abort
	end
end